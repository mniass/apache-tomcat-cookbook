default['apache-tomcat']['group'] = 'tomcat'
default['apache-tomcat']['user'] = 'tomcat'
default['apache-tomcat']['home_dir'] = '/opt/tomcat'
default['apache-tomcat']['java_home'] = 'usr/lib/jvm/java-7-openjdk-amd64/jre'
default['apache-tomcat']['java_opts'] = "-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"
default['apache-tomcat']['catalina_opts'] = "-Xms512M -Xmx1024M -server -XX:+UseParallelGC"
