#include_recipe 'apt'

execute 'Update cache' do 
 command 'sudo apt-get update'
end
package 'default-jdk'

group node['apache-tomcat']['group'] 

user node['apache-tomcat']['user'] do 
 home node['apache-tomcat']['home_dir']
 shell '/bin/false'
 group node['apache-tomcat']['group']
end

directory node['apache-tomcat']['home_dir']

cookbook_file "#{node['apache-tomcat']['home_dir']}/apache-tomcat-8.0.23.tar.gz" do
 source 'apache-tomcat-8.0.23.tar.gz'
 action :create
end

execute 'Extract file' do 
   command 'cd /opt/tomcat &&  sudo tar xvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1 && rm -rf apache-tomcat-8*tar.gz'
end

execute 'Grant user tomcat for repository' do
 command 'sudo chgrp -R tomcat /opt/tomcat/conf && sudo chmod g+rwx /opt/tomcat/conf && sudo chmod g+r /opt/tomcat/conf/* && sudo update-alternatives --config java' 
end

template '/etc/init/tomcat.conf' do 
 source 'tomcat.erb'
 owner 'root'
 group 'root'
 mode '0755'
end

execute 'Start tomcat ' do
 command 'sudo initctl reload-configuration'
end

service 'tomcat' do
 action [:start,:enable]
end


